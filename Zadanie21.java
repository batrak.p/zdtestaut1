package zestaw1;

import java.util.Scanner;

public class Zadanie21 {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        int n = scanner.nextInt();

        int[] binary = new int[32];

        if(n < 0) {
            binary[31] = 1;
            n = -n;
        }

        int i = 0;
        while(n > 0) {
            binary[i] = n % 2;
            n = n / 2;
            i++;
        }

        System.out.print("0.");
        boolean wasOne = false;
        boolean isPositive = binary[31] == 0;

        for(int j = 30; j >= 0; j--) {
            if(isPositive) {
                if(binary[j] == 0 && !wasOne) {
                    continue;
                } else {
                    wasOne = true;
                    System.out.print(binary[j]);
                }
            } else {
                if(binary[j] == 1 && !wasOne) {
                    continue;
                } else {
                    wasOne = true;
                    System.out.print(binary[j] == 0 ? 1 : 0);
                }
            }
        }
    }

}
