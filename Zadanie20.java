package zestaw1;

import java.util.Scanner;

public class Zadanie20 {

    public static void main(String[] args) {

        // -75 -> 1. 1 0 0 1 0 1 1
        //          64 0 0 8 0 2 1
        // int / + %

        int[] binary = new int[32];

        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();

        if(n < 0) {
            binary[31] = 1;
            n = -n;
        }

        // 1001011 -> >> 1  = 100101 = 37

        // 75 % 2 -> 1, 75/2 = 37
        // 37 % 2 -> 1, 37/2 = 18
        // 18 % 2 -> 0, 18/2 = 9
        // 9 % 2 -> 1, 9/2 = 4
        // 4 % 2 -> 0, 4/2 = 2
        // 2 % 2 -> 0, 2/2 = 1
        // 1 % 2 -> 1, 1/2 = 0

        int i = 0;
        while(n > 0) {
            binary[i] = n % 2;
            n = n / 2;
            i++;
        }

        boolean wasOne = false;
        System.out.print(binary[31] + ".");
        for(int j = 30; j >= 0; j--) {
            if(binary[j] == 0 && !wasOne) {
                continue;
            } else {
                wasOne = true;
                System.out.print(binary[j]);
            }
        }

    }

}
